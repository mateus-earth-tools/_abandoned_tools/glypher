//----------------------------------------------------------------------------//
//                       __      __                  __   __                  //
//               .-----.|  |_.--|  |.--------.---.-.|  |_|  |_                //
//               |__ --||   _|  _  ||        |  _  ||   _|   _|               //
//               |_____||____|_____||__|__|__|___._||____|____|               //
//                                                                            //
//  File      : CmdLineParser.cpp                                             //
//  Project   : glypher                                                           //
//  Date      : Sep 25, 2019                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2019                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//----------------------------------------------------------------------------//

// Header
#include "../include/CmdLineParser.h"
// std
#include <sstream>

//----------------------------------------------------------------------------//
// Public Methods                                                             //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
void
CmdLineParser::Parse(std::vector<std::string> const &args)
{
    auto const args_size = args.size();
    for(size_t i = 0; i < args_size; ++i) {
        auto const &arg     = args[i];
        auto const arg_type = _GetArgType(arg);

        // Non flags are easier... Just add them to the list.
        if(arg_type == ArgType::NonFlag) {
            m_nonFlags.push_back(arg);
            continue;
        }

        auto &flag = GetFlag(arg);
        flag.found = true;

        // Flag requires no values - Just continue the parsing.
        if(flag.valueType == ValueType::No) {
            continue;
        }

        // Flag might requires a value - So we need to process it.
        auto there_is_more_args = ((i + 1) < args_size);
        if(there_is_more_args) {
            auto const &next_arg      = args[i + 1];
            auto const next_arg_type = _GetArgType(next_arg);

            // The next arg is an argument for this flag.
            if(next_arg_type == ArgType::NonFlag) {
                flag.value = next_arg;
                ++i; // Consume next value...
            }
            // The next arg is actually a flag.
            else {
                // Flag requires an argument but we don't have it...
                if(flag.valueType == ValueType::Required) {
                    _FatalError_ArgumentRequired(flag);
                }
            }
        } else {
            // We don't have more values but the flag REQUIRES one.
            if(flag.valueType == ValueType::Required) {
                _FatalError_ArgumentRequired(flag);
            }
        }
    }
}

//------------------------------------------------------------------------------
CmdLineParser::Flag&
CmdLineParser::GetFlag(std::string const &name)
{
    auto clean_name = _CleanName(name);
    auto it = std::find_if(
        std::begin(m_flags),
        std::end  (m_flags),
        [&clean_name](Flag const &f){
            return clean_name == f.longName
                || clean_name == f.shortName;
        }
    );

    if(it == std::end(m_flags)) {
        // ERROR...
    }

    return *it;
}

//------------------------------------------------------------------------------
std::string
CmdLineParser::GenerateHelpString(std::string const &programName) const
{
    std::stringstream ss;
    ss << "Usage:\n";
    ss << "  " << programName << " ";

    for(auto const &flag : m_flags) {
        if(!flag.shortName.empty()) {
            ss << "[-" << flag.shortName;
        } else if(!flag.longName.empty()) {
            ss << "[--" << flag.longName;
        }

        if(flag.valueType == ValueType::Optional) {
            ss << " value";
        } else if(flag.valueType == ValueType::Required) {
            ss << " <value>";
        }
        ss << "] ";
    }

    ss << "\n\nOptions:\n";
    for(auto const &flag : m_flags) {
        ss << "  ";

        if(!flag.shortName.empty()) {
            ss << "-" << flag.shortName << " ";
        } else {
            ss << "   ";
        }
        if(!flag.longName.empty()) {
            ss << "--" << flag.longName << " ";
        }

        if(flag.valueType == ValueType::Optional) {
            ss << "value";
        } else if(flag.valueType == ValueType::Required) {
            ss << "<value>";
        }

        ss << " : " << flag.helpString << "\n";
    }
    return ss.str();
}

//----------------------------------------------------------------------------//
// Private Methods                                                            //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
std::string
CmdLineParser::_CleanName(std::string const &name) const
{
    auto type = _GetArgType(name.c_str());
    if(type == ArgType::NonFlag) {
        return name;
    }

    auto start_index = (type == ArgType::Short) ? 1 : 2;
    return name.substr(start_index);
}

//------------------------------------------------------------------------------
CmdLineParser::ArgType
CmdLineParser::_GetArgType(std::string const &arg) const
{
    auto len = arg.size();
    if(len <= 1) {
        return ArgType::NonFlag;
    } else if(len <= 2) {
        return (arg[0] == '-')
            ? ArgType::Short
            : ArgType::NonFlag;
    } else {
        return (arg[0] == '-' && arg[1] == '-')
            ? ArgType::Long
            : ArgType::NonFlag;
    }
}

//------------------------------------------------------------------------------
void
CmdLineParser::_FatalError_ArgumentRequired(Flag const &flag) const
{
    // @improve(stdmatt);
    std::stringstream ss;
    ss << "Flag (";
    if(!flag.shortName.empty() && flag.longName.empty()) {
        ss << "-" << flag.shortName;
    } else if(flag.shortName.empty() && !flag.longName.empty()) {
        ss << "--" << flag.longName;
    } else if(!flag.shortName.empty() && !flag.longName.empty()) {
        ss << "-" << flag.shortName;
        ss << " | ";
        ss << "--" << flag.longName;
    }
    ss << ") requires an argument.";

    printf("%s\n", ss.str().c_str());
    exit(1);
}
