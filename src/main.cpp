
// #include <string>

// #include "mcow/IO/printf.hpp"
// #include "mcow/StringUtils/StringUtils.hpp"
// #include "mcow/IO/Dir.hpp"
// #include "mcow/IO/Path.hpp"
// #include "mcow/IO/File.hpp"

// int main()
// {
//     auto dir = mcow::IO::Path::CurrentDirectory();
//     auto new_dir = mcow::IO::Path::Join(dir, {"new_directory"});

//     mcow::IO::Dir::Create(new_dir);
//     auto new_file = mcow::IO::Path::Join(dir, {"new_directory", "file.txt"});
//     auto p_file = mcow::IO::File::CreateText(new_file);
//     MCOW_SAFE_FCLOSE(p_file);
// }

// std
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <algorithm>
#include <iterator>
#include <vector>
#include <sstream>

// mcow
#include "mcow/Algo/Algo.hpp"
#include "mcow/CodeUtils/FakeKeywords.hpp"
#include "mcow/StringUtils/StringUtils.hpp"
#include "mcow/IO/Path.hpp"

// glypher
#include "../include/Graphics.h"
#include "../include/Packer.h"
#include "../include/Utils.h"
#include "../include/CmdLineParser.h"
#include "../include/Logger.h"


//----------------------------------------------------------------------------//
// Constants                                                                  //
//----------------------------------------------------------------------------//
constexpr auto PROGRAM_VERSION         = "0.0.1";
constexpr auto PROGRAM_NAME            = "glypher";
constexpr auto PROGRAM_COPYRIGHT_YEARS = "2019";


//----------------------------------------------------------------------------//
// Run Info                                                                   //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
class RunInfo
{
public:
    constexpr static auto GLYPHS_CLASS_ASCII   = "ascii";
    constexpr static auto GLYPHS_CLASS_ALPHA   = "alpha";
    constexpr static auto GLYPHS_CLASS_LOWER   = "lower";
    constexpr static auto GLYPHS_CLASS_UPPER   = "upper";
    constexpr static auto GLYPHS_CLASS_SPECIAL = "special";
    constexpr static auto GLYPHS_CLASS_DIGITS  = "digits";

    constexpr static uint8_t DEFAULT_FONT_SIZE  = 32;
    constexpr static auto    DEFAULT_GLYPHS_STR = GLYPHS_CLASS_ASCII;

    // Font
    uint8_t     fontSize;
    std::string fontFilename;

    // Glyphs
    std::string glyphsString;

    // Output
    std::string outputFilename;
}; // class RunInfo




//----------------------------------------------------------------------------//
// Helper Functions                                                           //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
mcow_internal_function void
ShowHelp(std::string const &str)
{
    printf("%s\n", str.c_str());
    exit(0);
}

//------------------------------------------------------------------------------
mcow_internal_function void
ShowVersion()
{
    printf("%s - %s - stdmatt <stdmatt@pixelwizards.io>\n", PROGRAM_NAME, PROGRAM_VERSION);
    printf("Copyright (c) %s - stdmatt\n", PROGRAM_COPYRIGHT_YEARS);
    printf("This is a free software (GPLv3) - Share/Hack it\n");
    printf("Check https://www.stdmatt.com for more :)\n");
    printf("\n");

    exit(0);
}

mcow_internal_function void
ParseCommandLine(RunInfo *pRunInfoOut, int argc, char *const argv[])
{
    mcow::Logger::SetLogLevel(mcow::Logger::LogLevel::LOG_LEVEL_NONE);

    // just to reduce vebosity...
    constexpr auto arg_no  = CmdLineParser::ValueType::No;
    constexpr auto arg_req = CmdLineParser::ValueType::Required;

    auto cmd_parser = CmdLineParser({
        {"help"         , "h", arg_no , "Show this screen."                       },
        {"version"      , "v", arg_no , "Show copyright and version info."        },
        {"verbose"      , "V", arg_no , "Extra logging."                          },
        {"font-size"    , "s", arg_req, "Size of the output font.",               },
        {"font-file"    , "f", arg_req, "TTF input font file.",                   },
        {"output-file"  , "o", arg_req, "The output filename.",                   },
        {"glyphs-string", "G", arg_req, "Glyphs that will be generated. (ascii,alpha,lower,upper,digits,special,custom)"}
    });

    cmd_parser.Parse({
        "-V",

        "--font-file",
        "/Users/stdmatt/Documents/Projects/stdmatt/personal/_fonts_/fonts/commodore_64/Commodore Rounded v1.2.ttf",

        "--font-size",
        "150",

        "--output-file",
        "./test.png",

        "--glyphs-string",
        "upper,digits"
    });


    //
    // Help / Version.
    if(cmd_parser.Found("help")) {
        ShowHelp(cmd_parser.GenerateHelpString(PROGRAM_NAME));
    }
    else if(cmd_parser.Found("version")) {
       ShowVersion();
    }

    //
    // Verbose.
    if(cmd_parser.Found("verbose")) {
        mcow::Logger::SetLogLevel(mcow::Logger::LogLevel::LOG_LEVEL_VERBOSE);
    }

    //
    // Font definitions.
    if(cmd_parser.Found("font-size")) {
        auto const& value = cmd_parser.GetFlag("font-size").value;
        if(!mcow::StringUtils::TryConvertTo<uint8_t>(value, &pRunInfoOut->fontSize)) {
            mcow::Logger::F("font-size expects an interger - found: (%s)", value);
        }
    } else {
        pRunInfoOut->fontSize = RunInfo::DEFAULT_FONT_SIZE;
    }

    if(cmd_parser.Found("font-file")) {
        auto const &value = cmd_parser.GetFlag("font-file").value;
        pRunInfoOut->fontFilename = value;
        if(!mcow::IO::Path::IsFile(pRunInfoOut->fontFilename)) {
            mcow::Logger::F("font-file expects a valid filename - found: (%s)", value);
        }
    } else {
        mcow::Logger::F("font-file must be specified...");
    }

    //
    // Glyph definitions.
    if(cmd_parser.Found("glyphs-string")) {
        auto const &value      = cmd_parser.GetFlag("glyphs-string").value;
        auto const &components = mcow::StringUtils::Split(value, ',');

        // @todo(stdmatt): Find a way to specify default classes with custom chars...
        for(auto const &component : components) {
            if(component == RunInfo::GLYPHS_CLASS_ASCII) {
                pRunInfoOut->glyphsString += BuildAsciiString();
            } else if(component == RunInfo::GLYPHS_CLASS_ALPHA) {
                pRunInfoOut->glyphsString += BuildAlphaString();
            } else if(component == RunInfo::GLYPHS_CLASS_LOWER) {
                pRunInfoOut->glyphsString += BuildLowerAlphaString();
            } else if(component == RunInfo::GLYPHS_CLASS_UPPER) {
                pRunInfoOut->glyphsString += BuildUpperAlphaString();
            } else if(component == RunInfo::GLYPHS_CLASS_DIGITS) {
                pRunInfoOut->glyphsString += BuildDigitsString();
            } else if(component == RunInfo::GLYPHS_CLASS_SPECIAL) {
                pRunInfoOut->glyphsString += BuildSpecialString();
            }
        }
    } else {
        pRunInfoOut->glyphsString += BuildAsciiString();
    }

    // We might have duplicated chars on the glyphs string.
    // This can occur when user specify overlaping character classes.
    // So here we just clean'em'up...
    mcow::Algo::Sort(pRunInfoOut->glyphsString);
    mcow::Algo::UniqueErase(pRunInfoOut->glyphsString);

    //
    // Output definitions.
    if(cmd_parser.Found("output-file")) {
        auto const &value = cmd_parser.GetFlag("output-file").value;
        pRunInfoOut->outputFilename = mcow::IO::Path::SplitExt(value).front();
    } else {
        mcow::Logger::F("output-file must be specified...");
    }
}


//----------------------------------------------------------------------------//
// Entry Point                                                                //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
int
main(int argc, char* argv[])
{
    RunInfo run_info;
    ParseCommandLine(&run_info, argc, argv);

    Graphics::Initialize();

    //
    // Load the font.
    auto p_font = Graphics::LoadFont(run_info.fontFilename, run_info.fontSize);

    //
    // Create glyphs.
    auto glyphs = std::vector<Graphics::Glyph_t>();
    glyphs.reserve(run_info.glyphsString.size());

    for(auto c : run_info.glyphsString) {
        Graphics::Glyph_t glyph;

        glyph.p_info    = Graphics::CreateGlyphInfo(c, p_font);
        glyph.p_texture = Graphics::CreateGlyphTexture(glyph.p_info);
        Graphics::GetTextureSize(glyph.p_texture, &glyph.rect.w, &glyph.rect.h);

        glyphs.push_back(glyph);
    }

    //
    // Bin Pack the Glyphs.
    auto rects_to_pack = std::vector<Rect_t *>();
    rects_to_pack.reserve(glyphs.size());

    for(auto &glyph : glyphs) {
        rects_to_pack.push_back(&glyph.rect);
    }

    constexpr auto PACKER_PADDING = 1;
    uint32_t spritesheet_width, spritesheet_height;
    Packer::Pack(rects_to_pack, PACKER_PADDING, &spritesheet_width, &spritesheet_height);

    //
    // Render the packed glyphs to output texture.
    auto p_output_texture = Graphics::CreateTexture(spritesheet_width, spritesheet_height);
    for(auto &glyph : glyphs) {
        Graphics::RenderTextureTo(
            glyph.p_texture,
            glyph.rect.x, glyph.rect.y,
            p_output_texture
        );
    }

    while(Graphics::IsWindowOpen()) {
        Graphics::WindowRunEvents();
        Graphics::WindowClear();
        Graphics::WindowPresent(p_output_texture);
    }

    Graphics::SaveTextureToFile(p_output_texture, run_info.outputFilename);
    // font_map_info.SaveToFile(run_info.outputFilename);

    for(auto &glyph : glyphs) {
        Graphics::DestroyTexture(glyph.p_texture);
        Graphics::DestroyGlyphInfo(glyph.p_info);
    }
    Graphics::DestroyTexture(p_output_texture);
    Graphics::DestroyFont   (p_font);

    Graphics::Shutdown();
}
