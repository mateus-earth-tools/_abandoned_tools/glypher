#pragma once

// mcow
#include "mcow/IO/printf.hpp"

namespace mcow { namespace Logger {

//------------------------------------------------------------------------------
enum LogLevel
{
    LOG_LEVEL_NONE = 0,

    LOG_LEVEL_DEBUG   = 1 << 0,
    LOG_LEVEL_INFO    = 1 << 1,
    LOG_LEVEL_WARNING = 1 << 2,
    LOG_LEVEL_ERROR   = 1 << 3,

    LOG_LEVEL_VERBOSE = 1 << 4,
}; // enum LogLevel

//------------------------------------------------------------------------------
void SetLogLevel   (Logger::LogLevel level);
void AddLogLevel   (Logger::LogLevel level);
void RemoveLogLevel(Logger::LogLevel level);

//------------------------------------------------------------------------------

template <typename ...Args>
inline void
D(char const * const pFormat, Args const & ... args)
{
    mcow::printf("[D] ");
    mcow::printf(pFormat, args...);
}

template <typename ...Args>
inline void
I(char const * const pFormat, Args const & ... args)
{
    mcow::printf("[I] ");
    mcow::printf(pFormat, args...);
}

template <typename ...Args>
inline void
W(char const * const pFormat, Args const & ... args)
{
    mcow::printf("[W] ");
    mcow::printf(pFormat, args...);
}

template <typename ...Args>
inline void
E(char const * const pFormat, Args const & ... args)
{
    mcow::printf("[E] ");
    mcow::printf(pFormat, args...);
}

template <typename ...Args>
inline void
F(char const * const pFormat, Args const & ... args)
{
    mcow::printf("[F] ");
    mcow::printf(pFormat, args...);
}

} // namespace Logger
} // namespace mcow
