#pragma once
// std
#include <stdint.h>
#include <vector>

struct Rect_t;

namespace Packer {

void Pack(
    std::vector<Rect_t*> rects,
    uint32_t             padding,
    uint32_t            *pResultWidthOut,
    uint32_t            *pResultHeightOut);

} // namespace Packer
